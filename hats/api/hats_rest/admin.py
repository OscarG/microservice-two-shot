from django.contrib import admin
from .models import Hat, LocationVO
# Register your models here.


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = [
        "imported_href",
        "closet_name",
    ]
