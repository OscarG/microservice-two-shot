# from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "imported_href",
        "closet_name",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "picture_url",
        "color",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(imported_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat_details(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse(
            {"message": count > 0},
            status=200,
        )
    else:
        content = json.loads(request.body)
        try:
            hat = Hat.objects.get(id=id)
            content == hat
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "invalid hat id"},
                status=400,
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_get_locations(request):
    if request.method == "GET":
        locations = LocationVO.objects.get()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationVODetailEncoder,
        )
