import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import HatDetail from './HatDetail';
import HatsForm from './HatsForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage /> }/>
          <Route path="/shoes" element={<ShoesList/>}/>
          <Route path="shoes/new" element={<ShoeForm/>} />
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatsForm />} />
            <Route path="detail/:id" element={<HatDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
