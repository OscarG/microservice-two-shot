import React, { useEffect, useState } from "react";

function HatsForm() {
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

    const handleFabricChange = (event) => {
        const {value} = event.target;
        setFabric(value);
    }
    const handleStyleChange = (event) => {
        const {value} = event.target;
        setStyle(value);
    }
    const handleColorChange = (event) => {
        const {value} = event.target;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const {value} = event.target;
        setPicture(value);
    }
    const handleLocationChange = (event) => {
        const {value} = event.target;
        setLocation(value);
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = style;
        data.color = color;
        data.picture_url = picture;
        data.location = location;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocation('');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return(
        <div className="my-3 container">
            <h1>Create a hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="mb-3">
                    <label htmlFor="Fabric" className="form-label">Fabric</label>
                    <input onChange={handleFabricChange} type="text" className="form-control"
                    id="fabric" placeholder="Fabric" value={fabric}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="Style" className="form-label">Style</label>
                    <input onChange={handleStyleChange} type="text" className="form-control"
                    id="style" placeholder="Style"/>
                </div>
                <div className="mb-3">
                    <label htmlFor="Color" className="form-label">Color</label>
                    <input onChange={handleColorChange} type="text" className="form-control"
                    id="color" placeholder="Color"/>
                </div>
                <div className="mb-3">
                    <label htmlFor="Picture" className="form-label">Picture URL</label>
                    <input onChange={handlePictureChange} type="text" className="form-control"
                    id="picture" placeholder="Picture URL"/>
                </div>
                <div className="mb-3">
                <select onChange={handleLocationChange} className="form-select" aria-label="Default select example">
                    <option value="">Open this select menu</option>
                    {locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                                {location.closet_name}
                            </option>
                        )
                    })}
                </select>
                </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
    );
}
export default HatsForm;
