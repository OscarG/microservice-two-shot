import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";

function HatList() {
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }
    const hatDelete = async (event) => {
        event.preventDefault();
        const hatID = event.target.value;
        console.log(`delete has been clicked with id: ${hatID}`);
        const hatUrl = `http://localhost:8090/api/hats/${hatID}/`
        const fetchConfig = {
            method: "delete",
            body: "",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            console.log("hat has been deleted");
            fetchData();
        } else {
            console.log("failed to delete hat");
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return(
        <div className="container my-3">
            <Link to="/hats/new">
                <button className="btn btn-primary">Add a hat</button>
            </Link>
        <div className="card-group my-3">
            {hats.map(hat => {
                return (
                    <div key={hat.id} className="card mb-3 shadow">
                        <img src={hat.picture_url} className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">{hat.style_name}</h5>
                            <p className="card-text">{hat.fabric}</p>
                            <h5 className="card-title">Location</h5>
                            <p className="card-text">{hat.location.closet_name}</p>
                            <h5 className="card-title-color">Color</h5>
                            <p className="card-text">{hat.color}</p>
                        </div>
                        <div className="card-footer">
                            <Link to={{
                                pathname: `/hats/detail/${hat.id}`
                            }}>
                                <button className="btn btn-primary mx-1">Hat details</button>
                            </Link>
                            <button onClick={hatDelete} value={hat.id} className="btn btn-danger mx-1">Delete</button>
                        </div>
                    </div>
                )
            })}
        </div>
        </div>
    )
}
export default HatList;
