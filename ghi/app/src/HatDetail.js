import React, { useEffect, useState} from "react";
import { useParams } from "react-router-dom";
import "./index.css";

function HatDetail() {
    const [hat, setHat] = useState('');
    const { id } = useParams();
    const fetchData = async () => {
        const url = `http://localhost:8090/api/hats/${id}/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHat(data);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="card my-3 hat-detail-img">
            <img src={hat.picture_url} className="card-img-top img-thumbnail" alt="..."/>
            <div className="card-body">
                <h5 className="card-title">{hat.style_name}</h5>
                <p className="card-text">Made with {hat.fabric}</p>
                <p className="card-text">Color {hat.color}</p>
                <p className="card-text">Located at {hat.location?.closet_name}</p>
            </div>
        </div>

    )
}
export default HatDetail;
