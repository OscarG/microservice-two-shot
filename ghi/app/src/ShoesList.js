import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export default function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        }
    };

    const delBtn = async (shoeId) => {
    console.log("Deleting shoe", shoeId);

    const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, { method: "delete" });
    console.log(response);
    if (response.ok) {
        getData();
    }
    };

    useEffect(() => {
    getData();
    }, []);


    return (
    <div>
        <h1>Your Shoes</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Color</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>pictureURL</th>
            <th>Bin</th>
            </tr>
        </thead>
        <tbody>
            {shoes.map(shoe => {
            return (
                <tr key={shoe.id}>
                <td>{shoe.color}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td><img width={200} src={shoe.pictureURL}  alt="" /></td>
                <td>{shoe.bin}</td>
                <td><button onClick={() => delBtn(shoe.id)} className="btn btn-danger">Delete</button></td>
                </tr>
            );
            })}
        </tbody>
        </table>
        <Link to = "/shoes/new">
        <button className="btn btn-primary">Add a shoe!</button>
        </Link>
    </div>
    );
}
