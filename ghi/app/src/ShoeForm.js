import React, { useState, useEffect } from 'react';

const BINS_API_URL = 'http://localhost:8100/api/bins/';
const SHOES_API_URL = 'http://localhost:8080/api/shoes/';

function ShoeForm() {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    pictureURL: '',
    });

    const fetchBinData = async () => {
    const response = await fetch(BINS_API_URL);
    if (response.ok) {
        const data = await response.json();
        setLocations(data.bins);
    }
    }

    useEffect(() => {
    fetchBinData();
    }, []);

    const handleSubmit = async (event) => {
    event.preventDefault();

        const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
        'Content-Type': 'application/json',
        },
    };

    const response = await fetch(SHOES_API_URL, fetchConfig);
    if (response.ok) {
        setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        pictureURL: '',
        bin: '',
        });
    }
    }

    const handleFormChange = (e) => {
    const { value, name } = e.target;
    setFormData(prevState => ({ ...prevState, [name]: value }));
    }

    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add a New Shoe</h1>
            <form onSubmit={handleSubmit} id="new-shoe-form">
            <FormInput id="manufacturer" name="manufacturer" value={formData.manufacturer} onChange={handleFormChange} label="Manufacturer" />
            <FormInput id="model_name" name="model_name" value={formData.model_name} onChange={handleFormChange} label="Model Name" />
            <FormInput id="color" name="color" value={formData.color} onChange={handleFormChange} label="Color" />
            <FormInput type="url" id="pictureURL" name="pictureURL" value={formData.pictureURL} onChange={handleFormChange} label="Picture URL" />
            <div className="mb-3">
                <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {locations.map(bin => (
                    <option key={bin.href} value={bin.href}>
                    {bin.closet_name}, Bin {bin.bin_number}
                    </option>
                ))}
                </select>
            </div>
            <button className="btn btn-primary">Submit</button>
            </form>
        </div>
        </div>
    </div>
    );
}

const FormInput = ({ id, name, value, onChange, type = "text", label }) => (
    <div className="form-floating mb-3">
    <input
        onChange={onChange}
        value={value}
        placeholder={label}
        required
        type={type}
        name={name}
        id={id}
        className="form-control"
    />
    <label htmlFor={id}>{label}</label>
    </div>
);

export default ShoeForm;
