# Wardrobify

Team:

- Person 1 - Which microservice?
  Jay Parfenchuck - shoes
- Person 2 - Which microservice?
  Oscar Gamez - Hats

## Design

For the hats design section I went with cards because it allowed me to put the image of the hats of the database as the centerpoint of each item being displayed, with information about each hat underneath the image.

For shoes I implimented a Form and List .js to enter and showcase the shoes. The Form consisted of adding the color, manufacturer, the model, pictureURL, and bin. The List would showcase all of this using a table in a row. The button on list.js will navigate you to Form.js to create shoe to add. The delete button will let you remove a shoe.

![wardrobify website layout](./wardrobify.png)

## Shoes microservice

Shoes api:
-Shoes list: http://localhost:8080/api/shoes/
GET request returns a list of shoes in the database
POST request posts a new shoe to the database

-Shoe details: http://localhost:8080/api/shoes/<id>/
-GET request returns the details of a shoe of the specified id
-PUT request updates a shoe at the specified id
-DELETE request deletes a shoe at the specified id

The backend will consist of two model classes. Those two models are BinVO and Shoes for the microservice.

- The BinVo model will consist of name, bin number, bin size, and import href.
- The Shoes model will consist of manufacturer, model name, color, and a picture URL.

These two work together by BinVO being used as a foreign key to Shoes. This is because the BinVO can store multiple shoes.
I also incorporated a poller that will be polling bin data from the wardrobe microservice. This is used to update and create BinVOs.
My frontend will be interacting with API endpoints to display the created shoe, a list of the shoes, and to delete a shoe.

## Hats microservice

Hat API:

-Hat List: http://localhost:8090/api/hats/
-GET request requests a list of all hats in the database
-POST request to add a hat to the database

-Hat Detail: http://localhost:8090/api/hats/<id>/
-GET request get the details of a hat in the database at a specific id
-PUT request updates a hat at the specified id
-DELETE request deletes a hat a specified id

The backend has two models, the hats model and the locationVO.

the Hats model tracks the fabric, style_name, color, picture_url, and location of the hat. The location of the hat is tied to the hat using a foreign key.

the hats microservice polls the wardrobe api for locations that are available and stores a reference to them in the LocationVO model. When adding a hat to the database from the HatForm component there is a dropdown menu that is populated from the locationVO locations.

My part of the frontend interacts with the hats api to pull a list of hats to display and information about those hats. I've also added a hats detail page that will show the information about the specific hat the user clicks on.
